﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; private set; }
        public decimal Sum { get; private set; }
        public bool IsParkingTransasction { get; private set; }

        public TransactionInfo(string vehicleId, decimal sum, bool isIncome) 
        {
            VehicleId = vehicleId;
            Sum = sum;
            IsParkingTransasction = isIncome;
        }
    }
}
