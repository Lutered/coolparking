﻿using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface
{
    static class ParkingServiceWrapper
    {
        private static readonly string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        private static ParkingService parkingService;
        public static ParkingService GetParkingService()
        {
            if (parkingService == null)
                parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService(_logFilePath));

            return parkingService;
        }
    }
}
