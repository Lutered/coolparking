﻿using CoolParking.Interface.Interfaces;
using System;
using System.Linq;

namespace CoolParking.Interface.Commands
{
    class EarnedMoneyCmd : ICommand
    {
        public void Execute()
        {
            string earnedMoney = ParkingServiceWrapper.GetParkingService().GetLastParkingTransactions().Where(x => x.IsParkingTransasction).Sum(x => Math.Abs(x.Sum)).ToString();
            Console.WriteLine($"Earned money for the current period: {earnedMoney}");
        }
    }
}
