﻿using CoolParking.BL.Models;
using CoolParking.Interface.Interfaces;
using System;

namespace CoolParking.Interface.Commands
{
    class AllTransCmd : ICommand
    {
        public void Execute()
        {
            TransactionInfo[] transactions = ParkingServiceWrapper.GetParkingService().GetLastParkingTransactions();
            Console.WriteLine("Transactions: ");

            if (transactions.Length == 0)
            {
                Console.WriteLine("No transactions found");
                return;
            }
            foreach (var transaction in transactions)
            {
                string trasactionType = transaction.IsParkingTransasction ? "withdraw" : "top-up";
                Console.WriteLine( $"There was {trasactionType} transaction for the amount {Math.Abs(transaction.Sum)} for vehicle with id {transaction.VehicleId}");
            }
        }
    }
}
