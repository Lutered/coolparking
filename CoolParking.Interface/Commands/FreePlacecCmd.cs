﻿using CoolParking.Interface.Interfaces;
using System;

namespace CoolParking.Interface.Commands
{
    class FreePlacecCmd : ICommand
    {
        public void Execute()
        {
            string freePlaces = ParkingServiceWrapper.GetParkingService().GetFreePlaces().ToString();
            string allPlaces = ParkingServiceWrapper.GetParkingService().GetCapacity().ToString();
            Console.WriteLine($"{freePlaces} free places on the parking place from {allPlaces}");
        }
    }
}
