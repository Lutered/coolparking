﻿using CoolParking.Interface.Interfaces;
using System;

namespace CoolParking.Interface.Commands
{
    class GetParkingBalanceCmd : ICommand
    {
        public void Execute()
        {
            string balance = ParkingServiceWrapper.GetParkingService().GetBalance().ToString();
            Console.WriteLine($"Parking balance: {balance}");
        }
    }
}
