﻿using CoolParking.Interface.Interfaces;
using System;

namespace CoolParking.Interface.Commands
{
    class GetLogCmd : ICommand
    {
        public void Execute()
        {
            try
            {
                string log = ParkingServiceWrapper.GetParkingService().ReadFromLog();
                Console.WriteLine("Log: ");
                Console.WriteLine(log);
            }
            catch
            {
                Console.WriteLine("Something goes wrong");
            }
        }
    }
}
