﻿using CoolParking.BL.Models;
using CoolParking.Interface.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.Interface.Commands
{
    class RemoveVehicleCmd : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Type vehicle id to remove:");
            string id = Console.ReadLine();
            ReadOnlyCollection<Vehicle> vehicles = ParkingServiceWrapper.GetParkingService().GetVehicles();
            Vehicle vehicleToRemove = vehicles.FirstOrDefault(x => x.id == id);
            if(vehicleToRemove == null)
            {
                Console.WriteLine($"Vehicle this id {id} was not found");
                return;
            }

            ParkingServiceWrapper.GetParkingService().RemoveVehicle(id);
            Console.WriteLine($"Vehicle with {id} was succesfully removed");
        }
    }
}
