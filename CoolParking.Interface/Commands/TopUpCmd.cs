﻿using CoolParking.BL.Models;
using CoolParking.Interface.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.Interface.Commands
{
    class TopUpCmd : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Type vehicle id to top up:");
            string id = Console.ReadLine();
            ReadOnlyCollection<Vehicle> vehicles = ParkingServiceWrapper.GetParkingService().GetVehicles();
            Vehicle vehicleToTopUp = vehicles.FirstOrDefault(x => x.id == id);
            if (vehicleToTopUp == null)
            {
                Console.WriteLine($"Vehicle this id {id} was not found");
                return;
            }

            Console.WriteLine("Type sum to top up:");
            string sumStr = Console.ReadLine();
            decimal sum;

            if (!decimal.TryParse(sumStr, out sum))
            {
                Console.WriteLine($"Sum should be a number");
                return;
            }

            if (sum <= 0)
            {
                Console.WriteLine($"Sum should be positive");
                return;
            }

            ParkingServiceWrapper.GetParkingService().TopUpVehicle(id, sum);
            Console.WriteLine($"The balance of vehicle {id} has been replenished in the amount {sum}");
        }
    }
}
