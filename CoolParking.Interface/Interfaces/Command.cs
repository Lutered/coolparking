﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface.Interfaces
{
    interface ICommand
    {
        void Execute();
    }
}
