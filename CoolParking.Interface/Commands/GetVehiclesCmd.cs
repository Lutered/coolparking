﻿using CoolParking.BL.Models;
using CoolParking.Interface.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.Interface.Commands
{
    class GetVehiclesCmd : ICommand
    {
        public void Execute()
        {
            ReadOnlyCollection<Vehicle> vehicles = ParkingServiceWrapper.GetParkingService().GetVehicles();
            Console.WriteLine("Vehicles: ");
            //Dictionary<VehicleType, string> vehicleType = new Dictionary<VehicleType, string>() 
            //{
            //    { VehicleType.PassengerCar, "PassengerCar" },
            //    { VehicleType.Truck, "Truck" },
            //    { VehicleType.Bus, "Bus"},
            //    { VehicleType.Motorcycle, "Motorcycle" }
            //};
            if (vehicles.Count() == 0)
                Console.WriteLine("Vehicles is not found");

            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{{ VehicleId: {vehicle.id}, VehicleType: {vehicle.VehicleType.ToString()}, Balance: {vehicle.Balance.ToString()} }}");
            }
        }
    }
}
