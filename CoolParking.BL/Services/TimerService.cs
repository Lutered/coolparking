﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        private Timer timer;

        public void Start()
        {
            Stop();

            timer = new System.Timers.Timer(Interval);
            timer.Elapsed += Elapsed;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        public void Stop()
        {
            if (timer != null)
            {
                timer.Close();
                timer = null;
            }
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
