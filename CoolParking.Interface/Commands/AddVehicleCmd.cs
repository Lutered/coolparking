﻿using CoolParking.BL.Models;
using CoolParking.Interface.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.Interface.Commands
{
    class AddVehicleCmd : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Input vehicle type: ");
            string vehicleTypeStr = Console.ReadLine();
            object vehicleType;

            if (!Enum.TryParse(typeof(VehicleType), vehicleTypeStr, out vehicleType))
            {
                Console.WriteLine("This vehicle type is not exist");
                return;
            }

            Console.WriteLine("Input balance: ");
            string balanceStr = Console.ReadLine();
            decimal balance;

            if (!Decimal.TryParse(balanceStr, out balance))
            {
                Console.WriteLine("balance is not a number");
                return;
            }

            string id = "";
            string generateCommand = "/generate";
            ReadOnlyCollection<Vehicle> vehicles = ParkingServiceWrapper.GetParkingService().GetVehicles();

            Console.WriteLine($"Input vehicle id or type {generateCommand} to generate random: ");
            string idCommand = Console.ReadLine();

            do
            {
                if (Vehicle.regex.IsMatch(idCommand) && vehicles.FirstOrDefault(x => x.id == idCommand) == null)
                {
                    id = idCommand;
                    break; 
                }            
                else if (idCommand == generateCommand)
                    id = Vehicle.GenerateRandomRegistrationPlateNumber();
                else
                {
                    if (Vehicle.regex.IsMatch(idCommand))
                        Console.WriteLine($"This id is already exist. Please type id again or type {generateCommand} to generate random:");
                    else
                        Console.WriteLine($"Your id have wrong format. Format for the id - AA-0001-AA. Please input id again or type {generateCommand} to generate random:");

                    idCommand = Console.ReadLine();
                }
            }
            while (vehicles.FirstOrDefault(x => x.id == id) != null || string.IsNullOrEmpty(id));

            ParkingServiceWrapper.GetParkingService().AddVehicle(new Vehicle(id, (VehicleType)vehicleType, balance));
            Console.WriteLine($"Vehicle was created with id {id}");
        }
    }
}
