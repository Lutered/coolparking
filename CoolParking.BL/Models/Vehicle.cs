﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public static readonly Regex regex = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
        public string id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }


        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!regex.IsMatch(id) || balance < 0)
                throw new ArgumentException();

            this.id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber() 
        {
            string id = "";
            string separator = "-";

            Random rng = new Random();
            List<(string, int)> list = new List<(string, int)>()
            {
                ("letter", 2),
                ("number", 4),
                ("letter", 2)
            };

            foreach (var item in list)
            {
                if (!string.IsNullOrEmpty(id))
                    id += separator;

                if (item.Item1 == "letter")
                {
                    for (int i = 0; i < item.Item2; i++)
                        id += (char)rng.Next(65, 91);
                }
                else if (item.Item1 == "number")
                {
                    for (int i = 0; i < item.Item2; i++)
                        id += rng.Next(0, 10);
                }
            }

            return id;
        }
    }
}
