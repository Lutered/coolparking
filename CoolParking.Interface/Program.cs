﻿using System;
using System.Collections.Generic;
using CoolParking.Interface.Interfaces;
using CoolParking.Interface.Commands;
using System.Linq;

namespace CoolParking.Interface
{
    class Program
    {
        internal static Dictionary<string, ICommand> commandDict = new Dictionary<string, ICommand>() 
        {
            {"/addvehicle", new AddVehicleCmd() },
            {"/getalltransactions", new AllTransCmd() },
            {"/getearnedmoney", new EarnedMoneyCmd() },
            {"/getfreeplaces", new FreePlacecCmd() },
            {"/getlog", new GetLogCmd() },
            {"/getparkbal", new GetParkingBalanceCmd() },
            {"/getvehicles", new GetVehiclesCmd() },
            {"/removevehicle", new RemoveVehicleCmd() },
            {"/topup", new TopUpCmd() }
        }; 
        static void Main(string[] args)
        {
            Console.WriteLine("Hello. This is console for parking management");
            Console.WriteLine("Commands: ");
            string cmd = "";
            string exitCmd = "/exit";
            string helpCmd = "/help";

            foreach (var item in commandDict.Keys)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine(helpCmd);
            Console.WriteLine(exitCmd);

            while (true) 
            {
                cmd = Console.ReadLine();

                if (cmd == exitCmd) break;
                if (cmd == helpCmd)
                {

                    foreach (var item in commandDict.Keys)
                    {
                        Console.WriteLine(item);
                    }
                    Console.WriteLine(exitCmd);
                    continue;
                }

                if (commandDict.Keys.Contains(cmd))
                {
                    commandDict[cmd].Execute();
                }
                else
                    Console.WriteLine("Unknown command");
            }
        }
    }
}
