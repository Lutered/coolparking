﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal startBalance = 0;
        public static int capacity = 10;
        public static int paymentPeriod = 5;
        public static int logPeriod = 60;
        public static double penaltyRate = 2.5;
        public static Dictionary<VehicleType, decimal> vehicleRate = new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.PassengerCar, 2m },
            { VehicleType.Truck, 5m},
            { VehicleType.Bus, 3.5m},
            { VehicleType.Motorcycle, 1m}
        };
    }
}
