﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parkingModel;
        private ILogService _logger;
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;

        private List<TransactionInfo> transactions = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService) 
        {
            _parkingModel = Parking.getInstance();
            _logger = logService;
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;

            _withdrawTimer.Elapsed += WithdrawVehiclesBalance;
            _withdrawTimer.Interval = Settings.paymentPeriod * 1000;
            _withdrawTimer.Start();

            _logTimer.Elapsed += WriteToLog;
            _logTimer.Interval = Settings.logPeriod * 1000;
            _logTimer.Start();
        }

        public decimal GetBalance() 
        {
            return _parkingModel.Balance;
        }
        public int GetCapacity() 
        {
            return Settings.capacity;
        }
        public int GetFreePlaces() 
        {
            int occupiedPlaces = _parkingModel.Vehicles.Count();
            return Settings.capacity - occupiedPlaces;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles() 
        {
            return new ReadOnlyCollection<Vehicle>(_parkingModel.Vehicles);
        }
        public void AddVehicle(Vehicle vehicle)
        {
            bool isVehicleExist = _parkingModel.Vehicles.FirstOrDefault(x => x.id == vehicle.id) != null;

            if (isVehicleExist)
                throw new ArgumentException();

            if (GetFreePlaces() <= 0) 
                throw new InvalidOperationException();


            _parkingModel.Vehicles.Add(vehicle);
        }
        public void RemoveVehicle(string vehicleId)
        {
            var vehicleList = _parkingModel.Vehicles;
            var vehicleToRemove = vehicleList.FirstOrDefault(x => x.id == vehicleId);

            if (vehicleToRemove == null)
                throw new ArgumentException();
            if (vehicleToRemove.Balance < 0)
                throw new InvalidOperationException();

            vehicleList.Remove(vehicleToRemove);
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
                throw new ArgumentException();

            var topUpVehicle = _parkingModel.Vehicles.FirstOrDefault(x => x.id == vehicleId);
            if (topUpVehicle == null)
                throw new ArgumentException();

            topUpVehicle.Balance += sum;
            transactions.Add(new TransactionInfo(vehicleId, sum, false));
        }
        public TransactionInfo[] GetLastParkingTransactions() 
        {
            TransactionInfo[] transactionArray = transactions.ToArray();
            return transactionArray;
        }
        public string ReadFromLog()
        {
            return _logger.Read();
        }
        public void WithdrawVehiclesBalance(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parkingModel.Vehicles)
            {
                decimal withdrawSum = Settings.vehicleRate[vehicle.VehicleType];
                decimal subtraction = vehicle.Balance - withdrawSum;

                if (subtraction < 0)
                {
                    if (Math.Abs(subtraction) >= withdrawSum)
                        withdrawSum *= (decimal)Settings.penaltyRate;
                    else
                    {
                        withdrawSum -= Math.Abs(subtraction);
                        withdrawSum += Math.Abs(subtraction) * (decimal)Settings.penaltyRate;
                    }
                }

                vehicle.Balance -= withdrawSum;
                _parkingModel.Balance += withdrawSum;
                transactions.Add(new TransactionInfo(vehicle.id, withdrawSum, true));
            }
        }

        public void WriteToLog(object sender, ElapsedEventArgs e)
        {
            string logInfo = "";
            foreach (var transaction in transactions)
            {
                if (!string.IsNullOrEmpty(logInfo))
                    logInfo += "\n";

                string trasactionType = transaction.IsParkingTransasction ? "withdraw" : "top-up";
                logInfo += $"There was {trasactionType} transaction for the amount {Math.Abs(transaction.Sum)} for vehicle with id {transaction.VehicleId}";       
            }
            transactions.Clear();
            //if(!string.IsNullOrEmpty(logInfo))
            _logger.Write(logInfo);
        }

        public void Dispose() 
        {
            WriteToLog(null, null);
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }
    }
}
